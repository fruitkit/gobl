# gobl

----

## **Go Obliterate!**

A Go file shredder.

----
### Requirements

* Go installed (v1.15+)
* urfave's cli library (v1)

----
### Install
`go get github.com/urfave/cli`

`cd path/to/gobl`

`go install`

----

### Usage

`gobl -h`

----

### FAQ

#### Windows or Linux?

Tested on Ubuntu 20.04 and Windows 10.
