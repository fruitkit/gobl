package main

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/urfave/cli"
)

func main() {
	var fNames cli.StringSlice
	var file string
	var wCount int
	var verbose bool
	var noDelete bool
	var b64Write bool
	var hexWrite bool
	var zeroWrite bool
	var rawWrite bool
	var noRawWrite bool
	var vSepBuf strings.Builder
	var vSepGrowth = "..."
	const goblDebug = false

	self := cli.NewApp()
	self.Name = "gobl"
	self.Usage = "go obliterate! gobl up all the files."
	self.Version = "1.1.0"

	self.Flags = []cli.Flag{
		cli.StringSliceFlag{
			Name:  "f, file",
			Usage: "Specify a file to delete. `FILE`",
			Value: &fNames,
		},
		cli.IntFlag{
			Name:        "c, count",
			Usage:       "Overwrite counter for EACH write type. `7`",
			Value:       7,
			Destination: &wCount,
		},
		cli.BoolFlag{
			Name:        "n, no-delete",
			Usage:       "Do NOT remove the files after overwrite.",
			Destination: &noDelete,
		},
		cli.BoolFlag{
			Name:        "w, no-raw",
			Usage:       "Do NOT overwrite files with raw bytes. (MUST be used with another write type)",
			Destination: &noRawWrite,
		},
		cli.BoolFlag{
			Name:        "b, base64",
			Usage:       "Overwrite files with base64 encoded data.",
			Destination: &b64Write,
		},
		cli.BoolFlag{
			Name:        "x, hex",
			Usage:       "Overwrite files with hex encoded data.",
			Destination: &hexWrite,
		},
		cli.BoolTFlag{
			Name:        "r, raw",
			Usage:       "Overwrite files with raw bytes data. (default: true)",
			Destination: &rawWrite,
		},
		cli.BoolFlag{
			Name:        "z, zero",
			Usage:       "Overwrite files with zero's.",
			Destination: &zeroWrite,
		},
		cli.BoolFlag{
			Name:        "V, verbose",
			Usage:       "Print to verbosely to stdout.",
			Destination: &verbose,
		},
	}

	self.Action = func(c *cli.Context) error {
		// fmt.Println("Args", c.Args(), "NArgs", c.NArg(), "os", os.Args)
		if len(fNames) < 1 {
			fmt.Println("Error: No files specified. Try using \"gobl --help\"")
			os.Exit(0)
		}
		if noRawWrite && rawWrite && !zeroWrite && !hexWrite && !b64Write {
			fmt.Println("Error: \"gobl -w\" MUST include at least one other write type. Try using \"gobl --help\"")
			os.Exit(0)
		}

		for _, file = range fNames {
			// Grab current file's info
			finfo, err := os.Stat(file)
			if os.IsNotExist(err) {
				fmt.Println("Error:", file, "does not exist.")
				continue
			} else if err != nil {
				log.Fatal(err)
			}

			fisf := finfo.IsDir()
			if fisf {
				fmt.Println("specified file:", file, "is a directory.")
				os.Exit(0)
			}

			// Byte size to overwrite
			fsz := int(finfo.Size())

			// Open file handle to overwrite
			fshred, err := os.OpenFile(
				file,
				os.O_WRONLY|os.O_CREATE,
				0666,
			)
			if err != nil {
				log.Fatal(err)
			}

			defer fshred.Close()

			if goblDebug {
				fmt.Println("Files to obliterate:", fNames)
			}
			w := 0
			for i := 0; i < wCount; i++ {

				// Byte array to store our buffer
				shredBytes := make([]byte, fsz+1)

				if zeroWrite {
					for z := range shredBytes {
						shredBytes[z] = 0
					}
					shredBytes = shredBytes[:fsz]
					fshred.WriteAt(shredBytes, 0)
					if verbose {
						vSepBuf.WriteString(vSepGrowth)
						fmt.Println(file, vSepBuf.String(), w+1)
					}
					w++
				}

				_, err = rand.Read(shredBytes)
				if err != nil {
					log.Fatal(err)
				}

				if rawWrite && !noRawWrite {
					fshred.WriteAt(shredBytes, 0)
					if verbose {
						vSepBuf.WriteString(vSepGrowth)
						fmt.Println(file, vSepBuf.String(), w+1)
					}
					w++
				}
				if b64Write {
					shredStr := base64.StdEncoding.EncodeToString(shredBytes)
					shredBytes = []byte(shredStr[:fsz])
					fshred.WriteAt(shredBytes, 0)
					if verbose {
						vSepBuf.WriteString(vSepGrowth)
						fmt.Println(file, vSepBuf.String(), w+1)
					}
					w++
				}
				if hexWrite {
					shredStr := hex.EncodeToString(shredBytes)
					shredBytes = []byte(shredStr[:fsz])
					fshred.WriteAt(shredBytes, 0)
					if verbose {
						vSepBuf.WriteString(vSepGrowth)
						fmt.Println(file, vSepBuf.String(), w+1)
					}
					w++
				}

			}

			// Remove file inode
			if !noDelete {
				_ = os.Remove(file)
			}

		}

		return nil

	}

	err := self.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
